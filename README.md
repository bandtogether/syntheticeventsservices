A script for generating synthetic events for testing the Band Together app.

* makeFakeData.py: the main script
    * requires the Python modules [numpy](http://www.numpy.org), [pandas](http://pandas.pydata.org), and [loremipsum](https://pypi.python.org/pypi/loremipsum/)
* veterans_location.csv: breakdowns by state (read by makeFakeData.py)
* img/: folder of event pics
* eventList.txt: the output of makeFakeData.py, in JSON format

And a lot of additional files with event info that can be incorporated into the script:

* manualevents.csv: a list of events that Jessica has made up by hand, with accompanying pictures stored in img/
