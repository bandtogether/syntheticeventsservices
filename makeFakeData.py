import datetime
import numpy as np
import pandas
import json

##
## Settings
##

numEvents = 9
numUsers = 2000

categories = ['volunteer', 'socialize', 'rejuvenate', 'compete']

outFileName = "eventList.txt"

locationFileName = "veterans_location.csv"

##
## Functions for generating fields
##

def chooseFromList(options, weights):
    """Choose one of N options given N weights"""
    probabilities = np.zeros(len(options)-1)
    for index in range(len(probabilities)):
        probabilities[index] = weights[index]/np.sum(weights)

    cutoffs = np.zeros_like(probabilities)
    for index in range(len(cutoffs)):
        cutoffs[index] = np.sum(probabilities[0:index+1])

    value = np.random.uniform()
    choice = options[len(options)-1]
    for index in np.array(range(len(cutoffs), 0, -1)) - 1:
        if (value < cutoffs[index]):
            choice = options[index]
    return choice

def getLocation():
    """Return a latitude and longitude"""
    l = pandas.read_csv(locationFileName)
    l.loc[l.State=='Alaska', 'LongH'] = -180 # hacky fix for Alaska wrapping around the IDL

    options = range(len(l))
    weights = [float(x) for x in l.Fraction]
    i = chooseFromList(options, weights)
    location = {
            'lat': (l.LatH[i]-l.LatL[i])*np.random.uniform() + l.LatL[i],
            'lon': (l.LongL[i]-l.LongH[i])*np.random.uniform() + l.LongH[i],
        }

    return location

def getDate():
    """Return a date for the event"""
    year = 2016
    daysToOmit = ['Jan 01', 'Nov 24', 'Dec 24', 'Dec 25', 'Jan 31']

    while 1:
        # see stackoverflow.com/questions/2427555
        dayInYear = np.random.randint(1, 365)
        date = datetime.datetime(year, 1, 1) + datetime.timedelta(dayInYear - 1)
        if not (date.strftime('%b %d') in daysToOmit):
            return date.strftime('%a, %b %d')

def getTime(date):
    """Return a plausible event time -- depends on day of week"""
    dayOfWeek = date[0:3]
    if dayOfWeek in ['Sat', 'Sun']:
        # random times from 10a to 8p
        time = datetime.datetime(2016, 1, 1, 10, 0)
        hh = np.random.randint(0, 10)
    else:
        # random times from 5p to 8p
        time = datetime.datetime(2016, 1, 1, 17, 0)
        hh = np.random.randint(0, 3)

    mm = 30*np.random.randint(0, 1)
    time = time + datetime.timedelta(seconds=3600*hh+60*mm)
    return time.strftime('%I:%M %p')

# set counters of events in each category to zero


def getDataByCategory(category):
    data = []
    titleList = []
    pictureList = []
    descriptionList = []
    if category == categories[0]:
        # volunteer
        titleList = ['Battle Buddies Dog Wash', 'Big Brothers and Big Sisters!']
        pictureList = ['img/dog-wash.jpg', 'img/BS-and-LS.jpg']
        descriptionList = ['Clean some dirty dogs for some clean money to benefit Battle Buddies!',
                           'Be a hero to a kid that needs one!']

    elif category == categories[1]:
        # socialize
        titleList = ['Appalachian Trail Hike', 'Spy School']
        pictureList= ['img/hiking-Jeff-Greenough.jpg', 'img/lrg_the_final_logo_small.jpg']
        descriptionList = ['Get a breath of fresh air.', 'Can you get through Spy School without blowing your cover?']

    elif category == categories[2]:
        # rejuvenate
        titleList = ['Battle Buddies Info Event', 'Hot Yoga', 'Intro Yoga']
        pictureList = ['img/battlebuddies.jpg', 'img/yoga-joes.jpg', 'img/veterans-2-yoag.jpg']
        descriptionList = ['Learn more about the Battle Buddies program and meet some of the dogs!',
                           'Can you take the heat?', 'Learn to be at one with the universe.']

    elif category == categories[3]:
        # compete
        titleList = ['Pickup Wheelchair Basketball', 'Mud Run']
        pictureList = ['img/120427_para_wheelchairbasketball.jpg', 'img/mud-run-obstacle-race.jpg']
        descriptionList = ['Come out and shoot some hoops!', 'After deployment this should be a piece of cake.']

    else:
        raise ValueError('unrecognized category value')

    data.append(titleList)
    data.append(pictureList)
    data.append(descriptionList)
    return data


def makeJsonArrayByCategory(category):
    data = getDataByCategory(category)
    titleList = data[0]
    pictureList = data[1]
    descriptionList = data[2]
    jsonArray = []
    for i in range(len(titleList)):
        event = {}
        event['title'] = titleList[i]
        event['picture'] = pictureList[i]
        event['description'] = descriptionList[i]
        event['category'] = category
        event['location'] = getLocation()
        event['date'] = getDate()
        event['time'] = getTime(event['date'])
        jsonArray.append(event)
    return jsonArray


def makeWholeArray():
    wholeArray = []
    for i in range(len(categories)):
        for data in makeJsonArrayByCategory(categories[i]):
            wholeArray.append(data)
    return wholeArray

##
## Write to file
##

with open(outFileName, 'w') as outFile:
    json.dump(makeWholeArray(), outFile)